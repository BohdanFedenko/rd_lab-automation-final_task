package com.epam.test.finaltask.rozetkatests.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ResultSearchPage extends BasePage {

    private static final String RESULT_PRODUCTS_LIST = "//a[contains(@class, 'goods-tile__heading')]/span";

    public ResultSearchPage(WebDriver driver) {
        super(driver);
    }

    @Step("Open a first product of the result search list")
    public ProductPage openProduct() {
        new WebDriverWait(driver, 100)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(RESULT_PRODUCTS_LIST)));
        driver.findElements(By.xpath(RESULT_PRODUCTS_LIST)).get(0).click();
        return new ProductPage(driver);
    }
}
