package com.epam.test.finaltask.rozetkatests.tests.modules.account;

import com.epam.test.finaltask.rozetkatests.pages.*;
import com.epam.test.finaltask.rozetkatests.utils.ConfProperties;
import com.epam.test.finaltask.rozetkatests.utils.TestRunnerHelper;
import com.epam.test.finaltask.rozetkatests.utils.listener.TestListener;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "User account")
public class AccountTest extends TestRunnerHelper {

    HomePage homePage;

    ResultSearchPage resultSearchPage;

    ProductPage productPage;

    UserAccountPage userAccountPage;

    RecentlyViewedProductPage recentlyViewedProductPage;

    @BeforeMethod
    public void setUp() {
        setUpConfig();
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        userAccountPage = new UserAccountPage(driver);
        recentlyViewedProductPage = new RecentlyViewedProductPage(driver);
        homePage
                .logIn()
                .inputEmail(ConfProperties.getProperty("user.email"))
                .inputPassword(ConfProperties.getProperty("user.password"))
                .logIn();
    }

    @Test(groups = "positive")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking account functions")
    @Story(value = "Recently viewed goods list")
    @Severity(value = SeverityLevel.NORMAL)
    public void showProductsInRecentlyViewedGoodsListTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String email = "testmailf2020@gmail.com";
        String userAccountPageTitle = "Личные данные";
        String recentlyViewedPageTitle = "Просмотренные товары";
        int viewedProductsListsSize = 1;
        int productNumber = 0;

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.openUserAccountPage();
        productPage.openUserAccountPage();
        asserts.checkOpenUserAccountPage(email, userAccountPageTitle);
        userAccountPage.openRecentlyViewedPage();

        //Then
        asserts.checkViewedProductsList(recentlyViewedPageTitle, productNumber, searchProduct, viewedProductsListsSize);
    }

    @AfterMethod
    public void tearDown() {
        recentlyViewedProductPage.clearList();
        tearDownConf();
    }
}
