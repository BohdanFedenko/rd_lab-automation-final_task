package com.epam.test.finaltask.rozetkatests.tests.modules.login;

import com.epam.test.finaltask.rozetkatests.pages.HomePage;
import com.epam.test.finaltask.rozetkatests.pages.LogInPage;
import com.epam.test.finaltask.rozetkatests.utils.TestRunnerHelper;
import com.epam.test.finaltask.rozetkatests.utils.listener.TestListener;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Login")
public class LoginModuleTest extends TestRunnerHelper {

    HomePage homePage;

    LogInPage logInPage;

    @BeforeMethod
    public void setUp() {
        setUpConfig();
        homePage = new HomePage(driver);
        logInPage = new LogInPage(driver);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking login with input an incorrect email template")
    @Story(value = "Login with an incorrect email template input")
    @Severity(value = SeverityLevel.BLOCKER)
    public void logInWithIncorrectEmailTemplateTest() {
        //Given
        String pageTitle = "Вход";
        String userName = "test@gmailcom";
        String password = "1Qte893TT";
        String errorMessage = "неверный адрес эл.почты";

        //When
        homePage.logIn();
        asserts.checkOpenLoginPage(pageTitle);
        logInPage.inputEmail(userName);
        logInPage.inputPassword(password);

        //Then
        asserts.checkErrorEmailTemplateMessage(errorMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking login with input an incorrect password")
    @Story(value = "Login with an incorrect password input")
    @Severity(value = SeverityLevel.BLOCKER)
    public void logInWithPasswordTest() {
        //Given
        String pageTitle = "Вход";
        String userName = "test@gmail.com";
        String password = "1Qte893TT";
        String errorMessage = "Введен неверный пароль!";

        //When
        homePage.logIn();
        asserts.checkOpenLoginPage(pageTitle);
        logInPage.inputEmail(userName);
        logInPage.inputPassword(password);
        logInPage.logIn();

        //Then
        asserts.checkErrorPasswordMessage(errorMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking login with input a password and an email are replaced")
    @Story(value = "Login with an input replaced password and email")
    @Severity(value = SeverityLevel.BLOCKER)
    public void logInWithReplacedValidPasswordAndEmail() {
        //Given
        String pageTitle = "Вход";
        String password = "TestMailF2020@gmail.com";
        String userName = "Iwtcirn2020";
        String errorEmailMessage = "неверный адрес эл.почты";

        //When
        homePage.logIn();
        asserts.checkOpenLoginPage(pageTitle);
        logInPage.inputEmail(userName);
        logInPage.inputPassword(password);
        logInPage.logIn();

        //Then
        asserts.checkErrorLoginMessages(errorEmailMessage);
    }

    @AfterMethod
    public void tearDown() {
        tearDownConf();
    }
}
