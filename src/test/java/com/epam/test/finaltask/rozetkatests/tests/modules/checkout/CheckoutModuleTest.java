package com.epam.test.finaltask.rozetkatests.tests.modules.checkout;

import com.epam.test.finaltask.rozetkatests.pages.*;
import com.epam.test.finaltask.rozetkatests.utils.TestRunnerHelper;
import com.epam.test.finaltask.rozetkatests.utils.listener.TestListener;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Checkout")
public class CheckoutModuleTest extends TestRunnerHelper {

    HomePage homePage;

    ResultSearchPage resultSearchPage;

    ProductPage productPage;

    ShoppingCartPage shoppingCartPage;

    CheckoutPage checkoutPage;

    @BeforeMethod
    public void setUp() {
        setUpConfig();
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
        checkoutPage = new CheckoutPage(driver);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking showing an error message of incorrect new customer name input")
    @Story(value = "Incorrect input to new customer name filed")
    @Severity(value = SeverityLevel.CRITICAL)
    public void checkoutWithIncorrectNewCustomerNameTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Оформление заказа";
        String newCustomerName = "Customer";
        String errorMessage = "Введите свое имя на кириллице";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.checkout();
        asserts.checkGoToCheckoutPageFromShoppingCart(pageTitle);
        checkoutPage.inputNewCustomerName(newCustomerName);

        //Then
        asserts.checkMessageOfInputIncorrectNewCustomerName(errorMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking showing an error message of incorrect new customer surname input")
    @Story(value = "Incorrect input to new customer surname filed")
    @Severity(value = SeverityLevel.CRITICAL)
    public void checkoutWithIncorrectNewCustomerSurnameTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Оформление заказа";
        String newCustomerSurname = "Customer";
        String errorMessage = "Введите свою фамилию на кириллице";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.checkout();
        asserts.checkGoToCheckoutPageFromShoppingCart(pageTitle);
        checkoutPage.inputNewCustomerSurname(newCustomerSurname);

        //Then
        asserts.checkMessageOfInputIncorrectNewCustomerSurname(errorMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking showing an error message of incorrect new customer phone number input")
    @Story(value = "Incorrect input to new customer phone number filed")
    @Severity(value = SeverityLevel.CRITICAL)
    public void checkoutWithIncorrectNewCustomerPhoneNumberTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Оформление заказа";
        String newCustomerPhoneNumber = "3568";
        String errorMessage = "Введите номер мобильного телефона";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.checkout();
        asserts.checkGoToCheckoutPageFromShoppingCart(pageTitle);
        checkoutPage.inputNewCustomerPhoneNumber(newCustomerPhoneNumber);

        //Then
        asserts.checkMessageOfInputIncorrectNewCustomerPhoneNumber(errorMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking showing an error message of submit checkout with empty fields")
    @Story(value = "Submit checkout with empty fields")
    @Severity(value = SeverityLevel.CRITICAL)
    public void checkoutWithEmptyFieldsTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Оформление заказа";
        String errorNameMessage = "Введите свое имя на кириллице";
        String errorSurnameMessage = "Введите свою фамилию на кириллице";
        String errorPhoneNumberMessage = "Введите номер мобильного телефона";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.checkout();
        asserts.checkGoToCheckoutPageFromShoppingCart(pageTitle);
        checkoutPage.submitOrder();

        //Then
        asserts.checkErrorMessages(errorNameMessage, errorSurnameMessage, errorPhoneNumberMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking showing an error message of an incorrect email input")
    @Story(value = "Incorrect input to exist customer email filed")
    @Severity(value = SeverityLevel.CRITICAL)
    public void checkoutIncorrectInputUserEmailTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Оформление заказа";
        String email = "TestMailF2020@gmailcom";
        String password = "1234";
        String errorEmailInputMessage = "Введен неверный адрес эл.почты или номер телефона";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.checkout();
        asserts.checkGoToCheckoutPageFromShoppingCart(pageTitle);
        checkoutPage.switchToExistCustomer();
        checkoutPage.inputExistCustomerEmail(email);
        checkoutPage.inputExistCustomerPassword(password);
        checkoutPage.clickLoginButton();

        //Then
        asserts.checkMessageOfInputIncorrectExistCustomerEmail(errorEmailInputMessage);
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking showing an error message of an incorrect password input")
    @Story(value = "Incorrect input to exist customer password filed")
    @Severity(value = SeverityLevel.CRITICAL)
    public void checkoutIncorrectInputPasswordTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Оформление заказа";
        String email = "TestMailF2020@gmail.com";
        String password = "1234";
        String errorPasswordInputMessage = "Введен неверный пароль!";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.checkout();
        asserts.checkGoToCheckoutPageFromShoppingCart(pageTitle);
        checkoutPage.switchToExistCustomer();
        checkoutPage.inputExistCustomerEmail(email);
        checkoutPage.inputExistCustomerPassword(password);
        checkoutPage.clickLoginButton();

        //Then
        asserts.checkMessageOfInputIncorrectExistCustomerPassword(errorPasswordInputMessage);
    }

    @AfterMethod
    public void tearDown() {
        tearDownConf();
    }
}
