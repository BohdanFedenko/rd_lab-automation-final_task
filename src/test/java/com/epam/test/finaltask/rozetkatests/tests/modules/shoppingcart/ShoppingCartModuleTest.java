package com.epam.test.finaltask.rozetkatests.tests.modules.shoppingcart;

import com.epam.test.finaltask.rozetkatests.pages.HomePage;
import com.epam.test.finaltask.rozetkatests.pages.ProductPage;
import com.epam.test.finaltask.rozetkatests.pages.ResultSearchPage;
import com.epam.test.finaltask.rozetkatests.pages.ShoppingCartPage;
import com.epam.test.finaltask.rozetkatests.utils.TestRunnerHelper;
import com.epam.test.finaltask.rozetkatests.utils.listener.TestListener;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Shopping Cart")
public class ShoppingCartModuleTest extends TestRunnerHelper {

    HomePage homePage;

    ResultSearchPage resultSearchPage;

    ProductPage productPage;

    ShoppingCartPage shoppingCartPage;

    @BeforeMethod
    public void setUp() {
        setUpConfig();
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
    }

    @Test(groups = "positive")
    @Flaky
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking adding a product to the Shopping Cart")
    @Story(value = "Add product to Shopping Cart")
    @Severity(value = SeverityLevel.CRITICAL)
    public void addProductToShoppingCartTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        int price = productPage.getProductMainPrice();
        productPage.addToShoppingCart();

        //Then
        asserts.checkAddProductToShoppingCart(price);
    }

    @Test(groups = "positive")
    @Flaky
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking deleting the product from the Shopping Cart")
    @Story(value = "Delete product from Shopping Cart")
    @Severity(value = SeverityLevel.CRITICAL)
    public void deleteProductFromShoppingCartTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String message = "Корзина пуста";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.openActionMenu();
        shoppingCartPage.removeProduct();

        //Then
        asserts.checkDeleteProductFromShoppingCart(message);
    }

    @Test(groups = "positive")
    @Flaky
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking changing quantity of the product on the Shopping Cart")
    @Story(value = "Change quantity of the products on the Shopping Cart")
    @Severity(value = SeverityLevel.NORMAL)
    public void changeQuantityProductFromShoppingCartTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        int quantityProducts = 2;

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        int productPrice = productPage.getProductMainPrice();
        productPage.addToShoppingCart();
        shoppingCartPage.changeUpProductQuantity();

        //Then
        asserts.checkChangeQuantityOfProductsOnShoppingCart(quantityProducts, productPrice);
    }

    @Test(groups = "positive")
    @Flaky
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking possibility to go to shopping again from the Shopping Cart")
    @Story(value = "Go to shopping gain from the Shopping Cart")
    @Severity(value = SeverityLevel.NORMAL)
    public void goToShoppingAgainFromShoppingCartTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.continueShopping();

        //Then
        asserts.checkGoToShoppingAgainFromShoppingCart();
    }

    @Test(groups = "positive")
    @Flaky
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking possibility to go to checkout from the Shopping Cart")
    @Story(value = "Go to checkout from the Shopping Cart")
    @Severity(value = SeverityLevel.BLOCKER)
    public void goToCheckoutFromShoppingCartTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Оформление заказа";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.checkout();

        //Then
        asserts.checkGoToCheckoutPageFromShoppingCart(pageTitle);
    }

    @AfterMethod
    public void tearDown() {
        tearDownConf();
    }
}
