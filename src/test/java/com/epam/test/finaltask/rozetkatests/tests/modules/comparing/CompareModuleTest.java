package com.epam.test.finaltask.rozetkatests.tests.modules.comparing;

import com.epam.test.finaltask.rozetkatests.pages.HomePage;
import com.epam.test.finaltask.rozetkatests.pages.ProductPage;
import com.epam.test.finaltask.rozetkatests.pages.ResultSearchPage;
import com.epam.test.finaltask.rozetkatests.utils.TestRunnerHelper;
import com.epam.test.finaltask.rozetkatests.utils.listener.TestListener;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Comparing products")
public class CompareModuleTest extends TestRunnerHelper {

    HomePage homePage;

    ResultSearchPage resultSearchPage;

    ProductPage productPage;


    @BeforeMethod
    public void setUp() {
        setUpConfig();
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
    }

    @Test(groups = "positive")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking comparing products")
    @Story(value = "Compare products")
    @Severity(value = SeverityLevel.NORMAL)
    public void comparingProductTest() {
        //Given
        String productNameOne = "Redmi note 9 Pro";
        String productNameTwo = "Galaxy M31s";
        String pageTitle = "Сравниваем мобильные телефоны";
        int productsQuantity = 2;

        //When
        homePage.search(productNameOne);
        resultSearchPage.openProduct();
        productPage.addToComparingList();
        productPage.search(productNameTwo);
        resultSearchPage.openProduct();
        productPage.addToComparingList();
        productPage.openComparingProductsPage();

        //Then
        asserts.checkAddedProductsToCompareList(pageTitle, productsQuantity);
    }

    @AfterMethod
    public void tearDown() {
        tearDownConf();
    }
}
