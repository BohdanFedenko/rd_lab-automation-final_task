package com.epam.test.finaltask.rozetkatests.utils;

import com.epam.test.finaltask.rozetkatests.tests.asserts.CustomAsserts;
import org.openqa.selenium.WebDriver;

public abstract class TestRunnerHelper {

    protected WebDriver driver;

    protected CustomAsserts asserts;

    DriverManager manager = new DriverManager();

    public WebDriver getDriver() {
        return driver;
    }

    public void setUpConfig() {
        driver = manager.startChromeDriver();
        asserts = new CustomAsserts(driver);
    }

    public void tearDownConf() {
        driver.close();
        driver.quit();
    }
}
