package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;

public class UserAccountPage extends BasePage {

    private static final String TITLE = "//h1[@class = 'cabinet__heading']";

    private static final String EMAIL_FIELD = "//p[@class = 'cabinet-user__email']";

    private static final String RECENTLY_VIEWED_BUTTON = "//ul[2]//a[contains(@href, 'recently-viewed')]";

    public UserAccountPage(WebDriver driver) {
        super(driver);
    }

    @Step("Open page with recently viewed products")
    public RecentlyViewedProductPage openRecentlyViewedPage() {
        SeleniumActionsAndExplicitWaits.click(driver, RECENTLY_VIEWED_BUTTON, 100);
        return new RecentlyViewedProductPage(driver);
    }

    public String getTitle() {
        return SeleniumActionsAndExplicitWaits.getText(driver, TITLE, 100);
    }

    public String getEmail() {
        return SeleniumActionsAndExplicitWaits.getText(driver, EMAIL_FIELD, 100);
    }
}
