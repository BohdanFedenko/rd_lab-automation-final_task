package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShoppingCartPage extends BasePage {

    private static final String CHECKOUT_BUTTON = "//a[contains(@class, 'cart-receipt__submit')]";

    private static final String CONTINUE_SHOP_BUTTON = "//a[contains(@class, 'cart-footer__continue')]";

    private static final String CLOSE_BUTTON = "//button[@class = 'modal__close']";

    private static final String TOTAL_PRICE = "//div[@class = 'cart-receipt__sum-price']";

    private static final String CART_ACTION_BUTTON = "//button[contains(@class, 'cart-actions__toggle')]";

    private static final String REMOVE_BUTTON = "//ul[@class = 'cart-actions__list']/li[1]";

    private static final String EMPTY_CART_MESSAGE = "//h4[@class = 'cart-dummy__heading']";

    private static final String ADD_ONE_MORE_PRODUCT_BUTTON = "//button[contains(@class, 'cart-counter')][2]";

    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }

    @Step("Continue shopping")
    public ProductPage continueShopping() {
        SeleniumActionsAndExplicitWaits.click(driver, CONTINUE_SHOP_BUTTON, 100);
        return new ProductPage(driver);
    }

    @Step("Change product quantity by {quantity}")
    public ShoppingCartPage changeUpProductQuantity() {
        SeleniumActionsAndExplicitWaits.click(driver, ADD_ONE_MORE_PRODUCT_BUTTON, 100);
        return this;
    }

    @Step("Open checkout page")
    public CheckoutPage checkout() {
        SeleniumActionsAndExplicitWaits.click(driver, CHECKOUT_BUTTON, 200);
        return new CheckoutPage(driver);
    }

    @Step("Open Shopping Cart action menu")
    public ShoppingCartPage openActionMenu() {
        SeleniumActionsAndExplicitWaits.click(driver, CART_ACTION_BUTTON, 100);
        return this;
    }

    @Step("Remove product")
    public ShoppingCartPage removeProduct() {
        SeleniumActionsAndExplicitWaits.click(driver, REMOVE_BUTTON, 100);
        return this;
    }

    @Step("Close Shopping Cart")
    public ProductPage close() {
        new WebDriverWait(driver, 100)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(CLOSE_BUTTON)));
        driver.findElement(By.xpath(CLOSE_BUTTON)).click();
        return new ProductPage(driver);
    }

    public String getEmptyShoppingCartMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, EMPTY_CART_MESSAGE, 100);
    }

    public int getTotalPrice() {
        return countPrice(getTotalCartPrice());
    }

    private String getTotalCartPrice() {
        return SeleniumActionsAndExplicitWaits.getText(driver, TOTAL_PRICE, 100);
    }

    private int countPrice(String price) {
        StringBuilder newPriceString = new StringBuilder();
        for (int i = 0; i < price.length(); i++) {
            if (Character.isDigit(price.charAt(i))) {
                newPriceString.append(price.charAt(i));
            }
        }
        return Integer.parseInt(newPriceString.toString());
    }
}
