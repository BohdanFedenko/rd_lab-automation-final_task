package com.epam.test.finaltask.rozetkatests.tests.asserts;

import com.epam.test.finaltask.rozetkatests.pages.*;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomAsserts {

    WebDriver driver;

    ProductPage productPage;

    ShoppingCartPage shoppingCartPage;

    CheckoutPage checkoutPage;

    WishListPage wishListPage;

    LogInPage logInPage;

    ComparingProductsPage comparingProductsPage;

    UserAccountPage userAccountPage;

    RecentlyViewedProductPage recentlyViewedProductPage;

    public CustomAsserts(WebDriver driver) {
        this.driver = driver;
        productPage = new ProductPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
        checkoutPage = new CheckoutPage(driver);
        wishListPage = new WishListPage(driver);
        logInPage = new LogInPage(driver);
        comparingProductsPage = new ComparingProductsPage(driver);
        userAccountPage = new UserAccountPage(driver);
        recentlyViewedProductPage = new RecentlyViewedProductPage(driver);
    }

    @Step("Check the name of opened product is equal {expProductName}")
    public void checkOpenProductPage(String expProductName) {
        assertThat(productPage).satisfies(page -> {
            assertThat(page.isProductAboutBriefDescription()).isTrue();
            assertThat(page.getProductName()).isNotEmpty();
            assertThat(page.getProductName()).contains(expProductName);
        });
    }

    @Step("Check the message of added to shopping cart is equal {message}")
    public void checkAddProductToShoppingCardMessage(String message) {
        assertThat(productPage).satisfies(page -> assertThat(page.getAddToShoppingCartMessage()).isEqualTo(message));
    }

    @Step("Check the added product to the Shopping Cart")
    public void checkAddProductToShoppingCart(int price) {
        assertThat(shoppingCartPage).satisfies(page -> assertThat(page.getTotalPrice() == price).isTrue());
    }

    @Step("Check deleting the product from the Shopping Cart by check the displayed message {message}")
    public void checkDeleteProductFromShoppingCart(String message) {
        assertThat(shoppingCartPage).satisfies(page -> assertThat(page.getEmptyShoppingCartMessage()).isEqualTo(message));
    }

    @Step("Check the quantity of the product to {quantity} on the Shopping Cart and total price {productPrice}")
    public void checkChangeQuantityOfProductsOnShoppingCart(int quantity, int productPrice) {
        assertThat(shoppingCartPage).satisfies(page -> assertThat(page.getTotalPrice() == productPrice * quantity).isTrue());
    }

    @Step("Check go to the checkout from the Shopping Cart by check the page title {title}")
    public void checkGoToCheckoutPageFromShoppingCart(String title) {
        assertThat(checkoutPage).satisfies(page -> assertThat(page.getTitle()).isEqualTo(title));
    }

    @Step("Check to continue shopping from the Shopping Cart")
    public void checkGoToShoppingAgainFromShoppingCart() {
        assertThat(productPage).satisfies(page -> {
            assertThat(page.isProductAboutBriefDescription()).isTrue();
            assertThat(page.getProductName()).isNotEmpty();
        });
    }

    @Step("Check add product to wish list with login")
    public void checkAddProductToWishList(String title, String productBrandName, int productNumber) {
        assertThat(wishListPage).satisfies(page -> {
            assertThat(page.getTitle()).isEqualTo(title);
            assertThat(page.getProductBrandName(productNumber)).isEqualTo(productBrandName);
        });
    }

    @Step("Check add product to wish list without login")
    public void checkAddProductToWishListWithoutLogin(String title) {
        assertThat(logInPage).satisfies(page -> assertThat(page.getTitle()).isEqualTo(title));
    }

    @Step("Check page title is equal to {pageTitle}")
    public void checkOpenLoginPage(String pageTitle) {
        assertThat(logInPage).satisfies(page -> assertThat(page.getTitle()).isEqualTo(pageTitle));
    }

    @Step("Check the message of an error email template is equal {message}")
    public void checkErrorEmailTemplateMessage(String message) {
        assertThat(logInPage).satisfies(page -> assertThat(page.getErrorEmailTemplateMessage()).contains(message));
    }

    @Step("Check the message of an error password")
    public void checkErrorPasswordMessage(String message) {
        assertThat(logInPage).satisfies(page -> assertThat(page.getErrorPasswordMessage()).isEqualTo(message));
    }

    @Step("Check the error login messages")
    public void checkErrorLoginMessages(String errorEmailMessage) {
        assertThat(logInPage).satisfies(page -> assertThat(page.getErrorEmailTemplateMessage()).contains(errorEmailMessage));
    }

    @Step("Check added products to the comparing list")
    public void checkAddedProductsToCompareList(String pageTitle, int productsQuantity) {
        assertThat(comparingProductsPage).satisfies(page -> {
            assertThat(page.getPageTitle()).isEqualTo(pageTitle);
            assertThat(page.getProductsQuantity() == productsQuantity).isTrue();
        });
    }

    @Step("Check open user account page")
    public void checkOpenUserAccountPage(String email, String title) {
        assertThat(userAccountPage).satisfies(page -> {
            assertThat(page.getTitle()).isEqualTo(title);
            assertThat(page.getEmail()).isEqualTo(email);

        });
    }

    @Step("Check viewed products list")
    public void checkViewedProductsList(String pageTitle, int productPosition, String productName, int listSize) {
        assertThat(recentlyViewedProductPage).satisfies(page -> {
            assertThat(page.getTitle()).isEqualTo(pageTitle);
            assertThat(page.getProductTitle(productPosition)).contains(productName);
            assertThat(page.getViewedProductListSize() == listSize).isTrue();
        });
    }

    @Step("Check a message of incorrect input new customer name")
    public void checkMessageOfInputIncorrectNewCustomerName(String message) {
        assertThat(checkoutPage).satisfies(page -> assertThat(page.getIncorrectInputNameMessage()).contains(message));
    }

    @Step("Check a message of incorrect input new customer surname")
    public void checkMessageOfInputIncorrectNewCustomerSurname(String message) {
        assertThat(checkoutPage).satisfies(page -> assertThat(page.getIncorrectInputSurnameMessage()).contains(message));
    }

    @Step("Check a message of incorrect input new customer phone number")
    public void checkMessageOfInputIncorrectNewCustomerPhoneNumber(String message) {
        assertThat(checkoutPage).satisfies(page -> assertThat(page.getIncorrectInputMobileNumberMessage()).contains(message));
    }

    @Step("Check a message of incorrect input an exist customer email")
    public void checkMessageOfInputIncorrectExistCustomerEmail(String message) {
        assertThat(checkoutPage).satisfies(page -> assertThat(page.getIncorrectInputUserNameMessage()).contains(message));
    }

    @Step("Check a message of incorrect input an exist customer password")
    public void checkMessageOfInputIncorrectExistCustomerPassword(String message) {
        assertThat(checkoutPage).satisfies(page -> assertThat(page.getIncorrectInputPasswordMessage()).contains(message));
    }

    @Step("Check error messages")
    public void checkErrorMessages(String errorNameMessage, String errorSurnameMessage, String errorPhoneNumberMessage) {
        assertThat(checkoutPage).satisfies(page -> {
            assertThat(page.getIncorrectInputNameMessage()).contains(errorNameMessage);
            assertThat(page.getIncorrectInputSurnameMessage()).contains(errorSurnameMessage);
            assertThat(page.getIncorrectInputMobileNumberMessage()).contains(errorPhoneNumberMessage);
        });
    }
}
