package com.epam.test.finaltask.rozetkatests.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[@name = 'search']")
    private WebElement searchField;

    @FindBy(xpath = "//button[contains(@class, 'search-form__submit')]")
    private WebElement searchButton;

    @FindBy(xpath = "//a[contains(@class, 'user-link link-dashed')]")
    private WebElement logInButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Step("Search product {productName}")
    public ResultSearchPage search(String productName) {
        new WebDriverWait(driver,50);
        searchField.clear();
        searchField.sendKeys(productName, Keys.ENTER);
        return new ResultSearchPage(driver);
    }

    @Step("Input product into search field {productName}")
    public HomePage inputIntoSearchField(String productName) {
        searchField.clear();
        searchField.sendKeys(productName);
        return this;
    }

    @Step("Click search button")
    public ResultSearchPage clickSearchButton() {
        searchButton.click();
        return new ResultSearchPage(driver);
    }

    @Step("Login to User Account Page")
    public LogInPage logIn() {
        logInButton.click();
        return new LogInPage(driver);
    }
}
