package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;

public class CheckoutPage extends BasePage {

    private static final String TITLE = "//h1[@class = 'checkout-heading']";

    private static final String NEW_CUSTOMER_BUTTON = "//li[1]/button[contains(@class, 'chips__button')]";

    private static final String EXIST_CUSTOMER_BUTTON = "//li[2]/button[contains(@class, 'chips__button')]";

    private static final String ORDER_SUBMIT_BUTTON = "//button[contains(@class, 'checkout-total__submit')]";

    private static final String NEW_CUSTOMER_MOBILE_NUMBER_FIELD = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname ='phone']";

    private static final String NEW_CUSTOMER_FIRST_NAME_FIELD = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname = 'name']";

    private static final String NEW_CUSTOMER_LAST_NAME_FIELD = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname ='surname']";

    private static final String EXIST_CUSTOMER_USER_NAME_FIELD = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname ='login']";

    private static final String EXIST_CUSTOMER_PASSWORD_FIELD = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname ='password']";

    private static final String LOGIN_BUTTON = "//button[contains(@class, 'auth-modal__submit')]";

    private static final String INCORRECT_INPUT_SURNAME_MESSAGE = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname = 'surname']/..//p";

    private static final String INCORRECT_INPUT_NAME_MESSAGE = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname = 'name']/..//p";

    private static final String INCORRECT_INPUT_MOBILE_NUMBER_MESSAGE = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname = 'phone']/..//p";

    private static final String INCORRECT_INPUT_USER_NAME_MESSAGE = "//fieldset[contains(@class, 'checkout-block_no_border')]//input[@formcontrolname ='login']/../p";

    private static final String INCORRECT_INPUT_PASSWORD_MESSAGE = "//div[@class = 'form__row form__hint form__hint_type_warning']/strong";

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    @Step("Switch to new customer")
    public CheckoutPage switchToNewCustomer() {
        SeleniumActionsAndExplicitWaits.click(driver, NEW_CUSTOMER_BUTTON, 100);
        return this;
    }

    @Step("Switch to exist customer")
    public CheckoutPage switchToExistCustomer() {
        SeleniumActionsAndExplicitWaits.click(driver, EXIST_CUSTOMER_BUTTON, 100);
        return this;
    }

    @Step("Input new customer name: {newCustomerName}")
    public CheckoutPage inputNewCustomerName(String newCustomerName) {
        SeleniumActionsAndExplicitWaits.sendKeysAndEnter(driver, NEW_CUSTOMER_FIRST_NAME_FIELD, newCustomerName, 100);
        return this;
    }

    @Step("Input new customer surname: {newCustomerSurname}")
    public CheckoutPage inputNewCustomerSurname(String newCustomerSurname) {
        SeleniumActionsAndExplicitWaits.sendKeysAndEnter(driver, NEW_CUSTOMER_LAST_NAME_FIELD, newCustomerSurname, 100);
        return this;
    }

    @Step("Input new customer phone number: {phoneNumber}")
    public CheckoutPage inputNewCustomerPhoneNumber(String phoneNumber) {
        SeleniumActionsAndExplicitWaits.sendKeysAndEnter(driver, NEW_CUSTOMER_MOBILE_NUMBER_FIELD, phoneNumber, 100);
        return this;
    }

    @Step("Input exist customer email: {email}")
    public CheckoutPage inputExistCustomerEmail(String email) {
        SeleniumActionsAndExplicitWaits.sendKeysAndEnter(driver, EXIST_CUSTOMER_USER_NAME_FIELD, email, 100);
        return this;
    }

    @Step("Input exist customer password: {password}")
    public CheckoutPage inputExistCustomerPassword(String password) {
        SeleniumActionsAndExplicitWaits.sendKeysAndEnter(driver, EXIST_CUSTOMER_PASSWORD_FIELD, password, 100);
        return this;
    }

    @Step("Click login button")
    public CheckoutPage clickLoginButton() {
        SeleniumActionsAndExplicitWaits.click(driver, LOGIN_BUTTON, 100);
        return this;
    }

    @Step("Submit order")
    public CheckoutPage submitOrder() {
        SeleniumActionsAndExplicitWaits.click(driver, ORDER_SUBMIT_BUTTON, 100);
        return this;
    }

    public String getIncorrectInputNameMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, INCORRECT_INPUT_NAME_MESSAGE, 100);
    }

    public String getIncorrectInputSurnameMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, INCORRECT_INPUT_SURNAME_MESSAGE, 100);
    }

    public String getIncorrectInputMobileNumberMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, INCORRECT_INPUT_MOBILE_NUMBER_MESSAGE, 100);
    }

    public String getIncorrectInputUserNameMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, INCORRECT_INPUT_USER_NAME_MESSAGE, 100);
    }

    public String getIncorrectInputPasswordMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, INCORRECT_INPUT_PASSWORD_MESSAGE, 100);
    }

    public String getTitle() {
        return SeleniumActionsAndExplicitWaits.getText(driver, TITLE, 100);
    }
}
