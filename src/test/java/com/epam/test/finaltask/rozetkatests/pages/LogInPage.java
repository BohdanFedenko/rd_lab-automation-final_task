package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogInPage extends BasePage {

    @FindBy(xpath = "//h3[@class = 'modal__heading']")
    private WebElement pageTitle;

    @FindBy(xpath = "//input[@id = 'auth_email']")
    private WebElement emailField;

    @FindBy(xpath = "//input[@id = 'auth_pass']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[contains(@class, 'auth-modal__submit')]")
    private WebElement loginButton;

    private static final String PAGE_TITLE = "//h3[@class = 'modal__heading']";

    private static final String ERROR_EMAIL_TEMPLATE_MESSAGE = "//p[@class = 'error-message']";

    private static final String ERROR_PASSWORD_MESSAGE = "//div[contains(@class, 'hint_type_warning')]/strong";

    public LogInPage(WebDriver driver) {
        super(driver);
    }

    @Step("Enter email {email}")
    public LogInPage inputEmail(String email) {
        emailField.clear();
        emailField.sendKeys(email);
        return this;
    }

    @Step("Enter password {password}")
    public LogInPage inputPassword(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
        return this;
    }

    @Step("Login")
    public UserAccountPage logIn() {
        loginButton.click();
        return new UserAccountPage(driver);
    }

    public String getTitle() {
        return  SeleniumActionsAndExplicitWaits.getText(driver, PAGE_TITLE, 100);
    }

    public String getErrorEmailTemplateMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, ERROR_EMAIL_TEMPLATE_MESSAGE, 100);
    }

    public String getErrorPasswordMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, ERROR_PASSWORD_MESSAGE, 100);
    }
}
