package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;

public class ProductPage extends BasePage {

    private static final String ADD_TO_SHOPPING_CART_BUTTON = "//app-buy-button[@class = 'toOrder'][@usershash = 'buy']";

    private static final String ADD_TO_SHOPPING_CART_MESSAGE = "//app-buy-button[@class = 'toOrder'][@usershash = 'buy']//span";

    private static final String OPEN_WISH_LIST_BUTTON = "//a[contains(@class, 'wish header')]";

    private static final String ADD_TO_COMPARING_LIST_BUTTON = "//button[contains(@class, 'compare-button')]";

    private static final String COMPARING_LIST_BUTTON = "//button[contains(@class, 'button_type_compare header')]";

    private static final String COMPARING_LIST_ITEM = "//a[contains(@class, 'comparison-modal__link')]";

    private static final String SEARCH_FIELD = "//input[@name = 'search']";

    private static final String PRODUCT_NAME = "//div[@class = 'product__heading']/h1";

    private static final String PRODUCT_MAIN_PRICE = "//p[contains(@class, 'product-prices__big')]";

    private static final String PRODUCT_ABOUT_BRIEF_DESCRIPTION = "//p[@class = 'product-about__brief']";

    private static final String OPEN_USER_ACCOUNT_BUTTON = "//a[contains(@class, 'user-link')]";

    private static final String ADD_TO_WISH_LIST_BUTTON = "//app-goods-wishlist[@class = 'product__favorites']/button";

    private static final String SHOPPING_CART_TITLE = "//h3[contains(@class, 'modal__heading')]";

    private static final String OPEN_SHOPPING_CART = "//a[contains(@class, 'basket header-actions')]";

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    @Step("Get product name")
    public String getProductName() {
        return SeleniumActionsAndExplicitWaits.getText(driver, PRODUCT_NAME, 100);
    }

    @Step("Add to Shopping Cart")
    public ShoppingCartPage addToShoppingCart() {
        SeleniumActionsAndExplicitWaits.click(driver, ADD_TO_SHOPPING_CART_BUTTON, 100);
        try {
            SeleniumActionsAndExplicitWaits.getText(driver, SHOPPING_CART_TITLE, 150);
        } catch (Exception e) {
            SeleniumActionsAndExplicitWaits.click(driver, OPEN_SHOPPING_CART, 50);
            return new ShoppingCartPage(driver);
        }
        return new ShoppingCartPage(driver);
    }

    @Step("Add to Wish List with login")
    public WishListPage addToWishListLoGinUser() {
        SeleniumActionsAndExplicitWaits.click(driver, ADD_TO_WISH_LIST_BUTTON, 50);
        return new WishListPage(driver);
    }

    @Step("Open Wish List page")
    public WishListPage openWishList() {
        SeleniumActionsAndExplicitWaits.click(driver, OPEN_WISH_LIST_BUTTON, 100);
        return new WishListPage(driver);
    }

    @Step("Add to Wish List without login")
    public LogInPage addToWishListLogOutUser() {
        SeleniumActionsAndExplicitWaits.click(driver, ADD_TO_WISH_LIST_BUTTON, 50);
        return new LogInPage(driver);
    }

    @Step("Add to comparing list")
    public ProductPage addToComparingList() {
        SeleniumActionsAndExplicitWaits.click(driver, ADD_TO_COMPARING_LIST_BUTTON, 100);
        return this;
    }

    @Step("Open Comparing Page")
    public ComparingProductsPage openComparingProductsPage() {
        SeleniumActionsAndExplicitWaits.click(driver, COMPARING_LIST_BUTTON, 100);
        SeleniumActionsAndExplicitWaits.click(driver, COMPARING_LIST_ITEM, 100);
        return new ComparingProductsPage(driver);
    }

    @Step("Search product {productName} from product page")
    public ResultSearchPage search(String productName) {
        SeleniumActionsAndExplicitWaits.sendKeysAndEnter(driver, SEARCH_FIELD, productName, 100);
        return new ResultSearchPage(driver);
    }

    @Step("Open user account page")
    public UserAccountPage openUserAccountPage() {
        SeleniumActionsAndExplicitWaits.click(driver, OPEN_USER_ACCOUNT_BUTTON, 100);
        return new UserAccountPage(driver);
    }

    public boolean isProductAboutBriefDescription() {
        return SeleniumActionsAndExplicitWaits.isDisplayed(driver, PRODUCT_ABOUT_BRIEF_DESCRIPTION, 100);
    }

    public String getAddToShoppingCartMessage() {
        return SeleniumActionsAndExplicitWaits.getText(driver, ADD_TO_SHOPPING_CART_MESSAGE, 100);
    }

    public int getProductMainPrice() {
        return countPrice(getMainPrice());
    }

    private String getMainPrice() {
        return SeleniumActionsAndExplicitWaits.getText(driver, PRODUCT_MAIN_PRICE, 100);
    }

    private int countPrice(String price) {
        StringBuilder newPriceString = new StringBuilder();
        for (int i = 0; i < price.length(); i++) {
            if (Character.isDigit(price.charAt(i))) {
                newPriceString.append(price.charAt(i));
            }
        }
        return Integer.parseInt(newPriceString.toString());
    }
}
