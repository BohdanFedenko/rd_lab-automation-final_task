package com.epam.test.finaltask.rozetkatests.utils;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverManager {

    private static final String OS_WINDOWS = "Windows 10";

    @Step("Open rozetka website")
    public WebDriver startChromeDriver() {
        setSystemPropertyForCurrentOS();
        WebDriver driver = getDriverOptions();
        driver.manage().window().maximize();
        driver.get(ConfProperties.getProperty("rozetka.website"));
        return driver;
    }

    private void setSystemPropertyForCurrentOS() {
        if (System.getProperty("os.name").equals(OS_WINDOWS)) {
            System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver.windows"));
        } else {
            System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver.linux"));
        }
    }

    private WebDriver getDriverOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("test-type");
        options.addArguments("--disable-notifications");
        options.addArguments("--disable-popup-blocking");
        return new ChromeDriver(options);
    }
}
