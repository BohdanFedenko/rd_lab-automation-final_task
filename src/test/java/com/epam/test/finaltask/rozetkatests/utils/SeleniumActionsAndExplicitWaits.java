package com.epam.test.finaltask.rozetkatests.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumActionsAndExplicitWaits {

    public static void click(WebDriver driver, String elementLocator, int time) {
        new WebDriverWait(driver, time)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(elementLocator)));
        driver.findElement(By.xpath(elementLocator)).click();
    }

    public static String getText(WebDriver driver, String elementLocator, int time) {
        new WebDriverWait(driver, time)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementLocator)));
        return driver.findElement(By.xpath(elementLocator)).getText();
    }

    public static boolean isDisplayed(WebDriver driver, String elementLocator, int time) {
        new WebDriverWait(driver, time)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementLocator)));
        return driver.findElement(By.xpath(elementLocator)).isDisplayed();
    }

    public static void sendKeysAndEnter(WebDriver driver, String elementLocator, String value, int time) {
        new WebDriverWait(driver, time)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(elementLocator)));
        driver.findElement(By.xpath(elementLocator)).clear();
        driver.findElement(By.xpath(elementLocator)).sendKeys(value, Keys.ENTER);
    }
}
