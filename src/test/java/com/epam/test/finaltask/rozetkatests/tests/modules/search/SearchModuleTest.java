package com.epam.test.finaltask.rozetkatests.tests.modules.search;

import com.epam.test.finaltask.rozetkatests.pages.HomePage;
import com.epam.test.finaltask.rozetkatests.pages.ProductPage;
import com.epam.test.finaltask.rozetkatests.pages.ResultSearchPage;
import com.epam.test.finaltask.rozetkatests.pages.ShoppingCartPage;
import com.epam.test.finaltask.rozetkatests.utils.TestRunnerHelper;
import com.epam.test.finaltask.rozetkatests.utils.listener.TestListener;
import io.qameta.allure.*;
import org.testng.annotations.*;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Searching")
public class SearchModuleTest extends TestRunnerHelper {

    HomePage homePage;

    ResultSearchPage resultSearchPage;

    ProductPage productPage;

    ShoppingCartPage shoppingCartPage;

    @BeforeMethod
    public void setUp() {
        setUpConfig();
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        shoppingCartPage = new ShoppingCartPage(driver);
    }

    @Test(groups = "positive")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking searching field by press an enter key")
    @Story(value = "input and start searching with an enter button")
    @Severity(value = SeverityLevel.CRITICAL)
    public void searchProductWithEnterButtonTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String addToSoppingCartMessage = "Товар уже в корзине";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.close();

        //Then
        asserts.checkAddProductToShoppingCardMessage(addToSoppingCartMessage);
    }

    @Test(groups = "positive")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking searching field by press an search button")
    @Story(value = "input and start searching with an search button")
    @Severity(value = SeverityLevel.CRITICAL)
    public void searchProductWithSearchButtonTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String addToSoppingCartMessage = "Товар уже в корзине";

        //When
        homePage.inputIntoSearchField(searchProduct);
        homePage.clickSearchButton();
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToShoppingCart();
        shoppingCartPage.close();

        //Then
        asserts.checkAddProductToShoppingCardMessage(addToSoppingCartMessage);
    }

    @AfterMethod
    public void tearDown() {
        tearDownConf();
    }
}
