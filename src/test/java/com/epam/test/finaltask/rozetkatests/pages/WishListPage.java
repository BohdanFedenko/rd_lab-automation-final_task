package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WishListPage extends BasePage {

    private static final String TITLE = "//h1[@class = 'cabinet__heading']";

    private static final String WISH_LIST_GOODS_TITLE = "//a[@class = 'goods-tile__heading']/span/strong";

    private static final String CHECK_ALL_PRODUCTS_BUTTON = "//button[contains(@class, 'check-all-goods')]";

    private static final String REMOVE_BUTTON = "//button[contains(@class, 'goods-delete')]";

    public WishListPage(WebDriver driver) {
        super(driver);
    }

    @Step("Check a brand name of the product number {productPositionNumber}")
    public String getProductBrandName(int productPositionNumber) {
        new WebDriverWait(driver, 100)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(WISH_LIST_GOODS_TITLE)));
        return driver.findElements(By.xpath(WISH_LIST_GOODS_TITLE)).get(productPositionNumber).getText();
    }

    @Step("Check all goods")
    public WishListPage checkAllGoods() {
        SeleniumActionsAndExplicitWaits.click(driver, CHECK_ALL_PRODUCTS_BUTTON, 100);
        return this;
    }

    @Step("Delete goods")
    public WishListPage delete() {
        SeleniumActionsAndExplicitWaits.click(driver, REMOVE_BUTTON, 100);
        return this;
    }

    public String getTitle() {
        return SeleniumActionsAndExplicitWaits.getText(driver, TITLE, 100);
    }
}
