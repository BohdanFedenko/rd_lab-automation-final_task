package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RecentlyViewedProductPage extends BasePage {

    private static final String TITLE = "//h1[@class = 'cabinet__heading']";

    private static final String MOVE_TO_HOME_PAGE_BUTTON = "//button[contains(@class, 'button button_size_medium')]";

    private static final String CLEAR_VIEWED_PRODUCT_LIST_BUTTON = "//button[contains(@class, 'viewed__clear')]";

    private static final String PRODUCT_LIST = "//a[@class = 'goods-tile__heading']/span";

    public RecentlyViewedProductPage(WebDriver driver) {
        super(driver);
    }

    @Step("Move to Home page")
    public HomePage moveToHomePage() {
        SeleniumActionsAndExplicitWaits.click(driver, MOVE_TO_HOME_PAGE_BUTTON, 100);
        return new HomePage(driver);
    }

    @Step("Clear viewed product list button")
    public RecentlyViewedProductPage clearList() {
        SeleniumActionsAndExplicitWaits.click(driver, CLEAR_VIEWED_PRODUCT_LIST_BUTTON, 100);
        return this;
    }

    @Step("Check title product number {productPositionNumber}")
    public String getProductTitle(int productPositionNumber) {
        new WebDriverWait(driver, 100)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(PRODUCT_LIST)));
        return driver.findElements(By.xpath(PRODUCT_LIST)).get(productPositionNumber).getText();
    }

    public int getViewedProductListSize() {
        return driver.findElements(By.xpath(PRODUCT_LIST)).size();
    }

    public String getTitle() {
        return SeleniumActionsAndExplicitWaits.getText(driver, TITLE, 100);
    }
}
