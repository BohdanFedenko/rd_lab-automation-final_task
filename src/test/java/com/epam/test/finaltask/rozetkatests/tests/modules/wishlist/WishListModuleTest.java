package com.epam.test.finaltask.rozetkatests.tests.modules.wishlist;

import com.epam.test.finaltask.rozetkatests.pages.*;
import com.epam.test.finaltask.rozetkatests.utils.ConfProperties;
import com.epam.test.finaltask.rozetkatests.utils.TestRunnerHelper;
import com.epam.test.finaltask.rozetkatests.utils.listener.TestListener;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
@Epic(value = "Rozetka website functional testing")
@Feature(value = "Wish list")
public class WishListModuleTest extends TestRunnerHelper {

    HomePage homePage;

    ResultSearchPage resultSearchPage;

    ProductPage productPage;

    WishListPage wishListPage;

    @BeforeMethod
    public void setUp() {
        setUpConfig();
        homePage = new HomePage(driver);
        resultSearchPage = new ResultSearchPage(driver);
        productPage = new ProductPage(driver);
        wishListPage = new WishListPage(driver);
    }

    @Test(groups = "positive")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking adding a product to the Wish List")
    @Story(value = "Add product to Wish List")
    @Severity(value = SeverityLevel.NORMAL)
    public void addProductToWishListWithLogInTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Список желаний";
        String productBrandName = "Xiaomi";
        int productNumber = 0;
        homePage
                .logIn()
                .inputEmail(ConfProperties.getProperty("user.email"))
                .inputPassword(ConfProperties.getProperty("user.password"))
                .logIn();

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        productPage.addToWishListLoGinUser();
        productPage.openWishList();

        //Then
        asserts.checkAddProductToWishList(pageTitle, productBrandName, productNumber);
        wishListPage
                .checkAllGoods()
                .delete();
    }

    @Test(groups = "negative")
    @Owner(value = "Bohdan Fedenko")
    @Description(value = "The test is checking adding a product to the Wish List without login")
    @Story(value = "Add product to Wish List without login")
    @Severity(value = SeverityLevel.NORMAL)
    public void addProductToWishListWithoutLogInTest() {
        //Given
        String searchProduct = "Redmi Note 9 Pro";
        String pageTitle = "Вход";

        //When
        homePage.search(searchProduct);
        resultSearchPage.openProduct();
        asserts.checkOpenProductPage(searchProduct);
        productPage.addToWishListLogOutUser();

        //Then
        asserts.checkAddProductToWishListWithoutLogin(pageTitle);
    }

    @AfterMethod
    public void tearDown() {
        tearDownConf();
    }
}
