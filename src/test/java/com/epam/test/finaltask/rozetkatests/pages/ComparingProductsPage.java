package com.epam.test.finaltask.rozetkatests.pages;

import com.epam.test.finaltask.rozetkatests.utils.SeleniumActionsAndExplicitWaits;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComparingProductsPage extends BasePage {

    private static final String TITLE = "//h1[@class = 'comparison__heading']";

    private static final String PRODUCTS_LIST = "//li[@class = 'products-grid__cell']";

    public ComparingProductsPage(WebDriver driver) {
        super(driver);
    }

    public String getPageTitle() {
        return SeleniumActionsAndExplicitWaits.getText(driver, TITLE, 100);
    }

    public int getProductsQuantity() {
        new WebDriverWait(driver, 100)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PRODUCTS_LIST)));
        return driver.findElements(By.xpath(PRODUCTS_LIST)).size();
    }
}
